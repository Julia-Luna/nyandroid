from datetime import datetime, timedelta
from discord import Embed

from .static import Staticstuff as s


def seconds_until_noon():
    now = datetime.utcnow()
    noon = datetime.utcnow().replace(hour=12, minute=0, second=0, microsecond=0)

    if now.hour >= 12:
        noon = noon + timedelta(days=1)

    delta = int(noon.timestamp() - now.timestamp())

    return delta


def make_spoiler(text):
    return "||" + text + "||"


def get_message_link(m):
    return f"https://discordapp.com/channels/{m.guild.id}/{m.channel.id}/{m.id}"


async def send_error(message, text="", command=""):
    embed = Embed(color=0xff2b48)
    err_msg = "**Error" + (f" in {command}" if command else "") + ":**"
    err_text = text
    embed.add_field(name=err_msg, value=err_text)
    await message.channel.send(embed=embed)
